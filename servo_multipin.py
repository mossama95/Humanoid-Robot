## another approach to control the motors using multipins instead of using decoder
## imports

import RPi.GPIO as GPIO
import time

## GPIO config.
GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)

## this function intializes all pins of servo motors
## list containig all servo pins locations

servo_pin = [4,17,27,22,5,6,13]
global pwm_list
pwm_list = []
def servoPinsInit():
    ## m1 -> pin 4
    ## m2 -> pin 17
    ## m3 -> pin 27
    ## m4 -> pin 22
    ## m5 -> pin 5
    ## m6 -> pin 6
    ## m7 -> pin 13
    
    ## set servo pins as output
    GPIO.setup(servo_pin[0],GPIO.OUT)
    GPIO.setup(servo_pin[1],GPIO.OUT)
    GPIO.setup(servo_pin[2],GPIO.OUT)
    GPIO.setup(servo_pin[3],GPIO.OUT)
    GPIO.setup(servo_pin[4],GPIO.OUT)
    GPIO.setup(servo_pin[5],GPIO.OUT)
    GPIO.setup(servo_pin[6],GPIO.OUT)
    

    ## intialize and start PWM signal on each pin of the servos woth 50 Hz frequncy
    

    pwm1 = GPIO.PWM(servo_pin[0],50)
    pwm1.start(0)
    pwm_list.append(pwm1)
    
    pwm2 = GPIO.PWM(servo_pin[1],50)
    pwm2.start(0)
    pwm_list.append(pwm2)
    
    pwm3 = GPIO.PWM(servo_pin[2],50)
    pwm3.start(0)
    pwm_list.append(pwm3)
    
    pwm4 = GPIO.PWM(servo_pin[3],50)
    pwm4.start(0)
    pwm_list.append(pwm4)
    
    pwm5 = GPIO.PWM(servo_pin[4],50)
    pwm5.start(0)
    pwm_list.append(pwm5)
    
    pwm6 = GPIO.PWM(servo_pin[5],50)
    pwm6.start(0)
    pwm_list.append(pwm6)
    
    pwm7 = GPIO.PWM(servo_pin[6],50)
    pwm7.start(0)
    pwm_list.append(pwm7)

    setAngle(3,90)
    setAngle(6,90)

## setAngle takes an angle and moves motor to this angle
## angle calculations varies from the motors as we have 2 different servos


def setAngle(mot_num,angle):
    if mot_num == 3 | mot_num == 6:  ## for the micro servos 1.8 kg
            DC = 1./20.* angle +2
    else:                             ## for the 13 kg servos
            DC = 1./18.* angle +1.5 
    pwm_list[mot_num-1].ChangeDutyCycle(DC)
    time.sleep(0.5)
        
def moveMotor(mot_num,angle):
    
    ## note that angles must be checked
    ## for the left arm "motors 1,2,3" we use the angle directly
    ## but for the right arm "motors 4,5,6" the angle is inverted
    ## for the micro servos "motors 3,6" angle should be between 0 and 90

    ## for safety

    if angle > 179:
        angle = 179
    if angle < 1:
        angle = 1

        
    if mot_num in [4,5,6]: ## left arm
        if mot_num == 6 & angle >90 :
            setAngle(mot_num,180)
        elif mot_num == 6:
            setAngle(6,90+angle)
        else:       
            setAngle(mot_num,180-angle)
                    
    else:                  ## right arm
        if mot_num == 3 & angle >90 :
            setAngle(mot_num,0)
        elif mot_num == 3:
            setAngle(3,90-angle)
        else:
            setAngle(mot_num,angle)

## testing
servoPinsInit()
"""
while True:
    moveMotor(1,90)
    moveMotor(5,30)
    moveMotor(3,20)
    time.sleep(0.5)
    print ("om")
    moveMotor(4,90)
    moveMotor(2,30)
    moveMotor(6,20)
    time.sleep(0.5)
    
    moveMotor(1,0)
    moveMotor(5,0)
    moveMotor(3,0)
    time.sleep(0.5)
    
    
    moveMotor(4,60)
    moveMotor(2,10)
    moveMotor(6,60)
    time.sleep(0.5)

    
    moveMotor(1,60)
    moveMotor(5,10)
    moveMotor(3,60)
    time.sleep(0.5)
    
    moveMotor(4,0)
    moveMotor(2,0)
    moveMotor(6,0)
    time.sleep(0.5)


"""
"""
    angle = int(test_input)
    moveMotor(1,a)
    time.sleep(1)
    print(n)
    print(a)
"""
"""
#GPIO.cleanup()
for i in range (8):
    moveMotor(i,30)
    print(i)
    time.sleep(2)
GPIO.cleanup()

"""
