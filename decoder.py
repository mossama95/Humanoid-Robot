import RPi.GPIO as GPIO
import time
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

global A0,A1,A2  ## decoder select pins
A0 = 5
A1 = 6
A2 = 13

## set pins 5,6,13 as output for decoder select

GPIO.setup(A0,GPIO.OUT)
GPIO.setup(A1,GPIO.OUT)
GPIO.setup(A2,GPIO.OUT)


def selectMotor (mot_num):
    mot_num_bin = '{:03b}'.format(mot_num) ## returns a string with resolution = 3 containing motor number in binrayprint (mot_num_bin[0])

    GPIO.output(A0, int(mot_num_bin[2]))
    GPIO.output(A1, int(mot_num_bin[1]))
    GPIO.output(A2, int(mot_num_bin[0]))

    
## testing
"""

GPIO.setup(17,GPIO.OUT)
GPIO.output(17,GPIO.HIGH)
while True :
    number = int(input("enter number: "))
    selectMotor(number)
""" 
