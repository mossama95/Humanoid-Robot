## authors Mohamad Ossama and Aya Emad
from multiprocessing import Queue
import string
import random
import threading
from servo import *


threadLock = threading.Lock() ## new instace for thread lock
global dataBuffer ## Global Queue to store all commands to be applied to motors
dataBuffer = Queue()

###############################################################################

## this function generates random msgs that can be a protocol with vision team
## m_motorNumber_a_angle

def randomMsg():
    angle = random.randint(1,181) ## angle between 1 and 180
    motNum  = random.randint(1,8) ## motor number between 1 and 7
    msg = 'm_'+str(motNum)+'_a_'+str(angle)
    return msg

################################################################################

## this function pushes the data buffer with 100 random commands to test the other functions

def push_into_buffer():
    #while True:
    dataBuffer.put("m_2_a_0")
    for i in range(0,180,10):
        threadLock.acquire()
        
        msg = "m_1_"+"a_"+str(i)
        dataBuffer.put(msg)
        #print("push")
        threadLock.release()
        """
        threadLock.acquire()
        dataBuffer.put(randomMsg())
        #print("push")
        threadLock.release()
        """
        
################################################################################
        
## assume that recieved data to be stored in buffer will be on the following structure
## m_NumMotor_a_angle
## this function is used to process data of the input buffer and returns motNum to move and an angle
def process_buffer():
    #print("this is thread 2\n")
    threadLock.acquire()
    msg = dataBuffer.get()          ## msg = 'm_motNum_a_angle'
    msgContent = msg.split("_")     ## msgContent = ['m','motNum','a','angle']
    motNum = int(msgContent[1])
    angle = int(msgContent[3])
    ##print("angle ",angle)
    ##print ("motor ",motNum )
    ##print(dataBuffer.qsize())
    threadLock.release()
    #time.sleep(0.5)
    return motNum,angle


## a function that always runs and calls the processing function
def main():
    while True:
        mot_num , angle = process_buffer()
        setAngle(angle)
        print("angle ",angle)
        """
        print("angle ",angle)
        print ("motor ",mot_num )
        """
        
    
