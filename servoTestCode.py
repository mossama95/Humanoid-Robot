import RPi.GPIO as GPIO
import time





def servo_init(servoPin = 17): 
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(servoPin,GPIO.OUT)
    global PWM
    PWM = GPIO.PWM(servoPin,50)
    PWM.start(2.5)
    servo_set_angle(0)

def servo_set_angle(angle):
    DC = (angle/180.0+1.0)*5.0
    PWM.ChangeDutyCycle(DC)
    time.sleep(2)

def servo_stop():
    servo_set_angle(0)
    PWM.stop()
    GPIO.cleanup()


## testing
while True:
    servo_init()   
    testAngle = input("enter angle: ")
    if int(testAngle) == 66:
        break
    servo_set_angle(int(testAngle))
    
    servo_stop()
			

GPIO.cleanup()
