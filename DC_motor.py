import RPi.GPIO as GPIO
import time 
## motor 1 pins 16,23
## motor 2 pins 24,25
## motor 1 enable  pin 7
## motor 2 enable  PIN 8

## enable PWM signal on enable pins so we can cnotrol its speed

speed = [30,65,100]
current_speed = 0

        
GPIO.setmode(GPIO.BCM)
GPIO.setup(16,GPIO.OUT)
GPIO.setup(23,GPIO.OUT)
GPIO.setup(24,GPIO.OUT)
GPIO.setup(25,GPIO.OUT)
GPIO.setup(7,GPIO.OUT)
GPIO.setup(8,GPIO.OUT)
pwm_M1 = GPIO.PWM(7,100)

pwm_M2 = GPIO.PWM(8,100)
## start at 80 % of the speed
pwm_M1.start(80)

pwm_M1.start(80)

def forward(tf):
	init()
	GPIO.output(16,False)
	GPIO.output(23,True)
	GPIO.output(24,True)
	GPIO.output(25,False)
	time.sleep(tf)
	GPIO.cleanup()

def reverse(tf):
	init()
	GPIO.output(16,True)
	GPIO.output(23,False)
	GPIO.output(24,False)
	GPIO.output(25,True)
	time.sleep(tf)
	GPIO.cleanup()

def turn_left(tf):
	init()
	GPIO.output(16,True)
	GPIO.output(23,True)
	GPIO.output(24,True)
	GPIO.output(25,False)
	time.sleep(tf)
	GPIO.cleanup()

def turn_right(tf):
	init()
	GPIO.output(16,False)
	GPIO.output(23,True)
	GPIO.output(24,False)
	GPIO.output(25,False)
	time.sleep(tf)
	GPIO.cleanup()
	
def speed_up():
        global current_speed
        global speed
        global pwm_M1
        global pwm_M2
        if current_speed < 2:
                current_speed = current_speed+1
        
        pwm_M1.ChangeDutyCycle(speed[current_speed])
        pwm_M2.ChangeDutyCycle(speed[current_speed])
        print("current speed :"+str(speed[current_speed]))
        
def speed_down():
        global current_speed
        global speed
        
        global pwm_M1
        global pwm_M2
        
        if current_speed >0:
                current_speed = current_speed-1
        pwm_M1.ChangeDutyCycle(speed[current_speed])
        pwm_M2.ChangeDutyCycle(speed[current_speed])
        print("current speed :"+str(speed[current_speed]))

        
## testing
while True:
        speed_up()
        
        speed_up()
        
        speed_up()
        
        speed_up()

        speed_down()
        
        speed_down()
        speed_down()
        speed_down()
	
