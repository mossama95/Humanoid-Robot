import threading
import time
from myFunctions import *

class myThread (threading.Thread):
    def __init__(self,threadID,name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name

    def run(self):
        print ("satring thread, \n" , self.threadID )
        if self.threadID == 1:
            ## thread 1 that pushes into buffer
            push_into_buffer()
        elif self.threadID == 2:
            ## thread 2 main function to process data 
            main()
        elif self.threadID == 3:
            ## thread 3 that moves the motors to desired angle
            move_motor()
