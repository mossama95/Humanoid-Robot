import RPi.GPIO as GPIO
from decoder import *
import time
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
servo_pin = 17
GPIO.setup(servo_pin,GPIO.OUT)
my_pwm = GPIO.PWM(servo_pin,50)
my_pwm.start(0)

## setAngle takes an angle and moves motor to this angle
## angle calculations varies from the motors as we have 2 different servos

def setAngle(mot_num,angle):
        if mot_num == 3 | mot_num == 6:  ## for the micro servos 1.8 kg
                DC = 1./20.* angle +2
        else:                             ## for the 13 kg servos
                DC = 1./18.* angle +1.5 
        my_pwm.ChangeDutyCycle(DC)
        time.sleep(0.5)

def moveMotor(mot_num,angle):
        ## selects the output of the decoder to the motor
        selectMotor(mot_num)
        
        ## note that angles must be checked
        ## for the left arm "motors 1,2,3" we use the angle directly
        ## but for the right arm "motors 4,5,6" the angle is inverted
        ## for the micro servos "motors 3,6" angle should be between 0 and 90

        if mot_num in [4,5,6]:
                if mot_num == 6 & angle >90 :
                        setAngle(mot_num,90)
                else:       
                        setAngle(mot_num,180-angle)
        else:
                if mot_num == 6 & angle >90 :
                        setAngle(mot_num,90)
                else:
                        setAngle(mot_num,angle)
        
#testing
"""
while True:
	DC = (float(input("enter angle ")))
	
        
	setAngle(1,DC)
			
my_pwm.stop()
GPIO.cleanup()

"""
"""
while True:
        test_input = str(input("enter motor and angle : "))
        n = int((test_input.split(" "))[0])
        a = int((test_input.split(" "))[1])
        
        moveMotor(n,a)
        time.sleep(1)
        print(n)
        print(a)
        ##moveMotor(2,0)
        ##time.sleep(1)
        ##print("2")
        ##moveMotor(3,0)

        ##time.sleep(1)
        ##print("3")
"""
